import json
from datetime import datetime
from typing import Iterable

from app.bookings.repository import BookingsRepo
from app.hotels.repository import RoomsRepo, HotelsRepo
from app.logger import logger

TABLE_MODEL_MAP = {
    "hotels": HotelsRepo,
    "rooms": RoomsRepo,
    "bookings": BookingsRepo
}


def convert_from_csv_to_postgres(csv_iter: Iterable):
    try:
        data = []
        for row in csv_iter:
            for k, v in row.items():
                if v.isdigit():
                    row[k] = int(v)
                elif k == "services":
                    row[k] = json.loads(v.replace("'", '"'))
                elif "date" in k:
                    row[k] = datetime.strptime(v, "%Y-%m-%d")
            data.append(row)
        return data
    except Exception:
        logger.error("Cannot convert CSV into DB format", exc_info=True)
