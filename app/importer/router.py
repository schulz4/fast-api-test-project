import codecs
import csv
from typing import Literal

from fastapi import APIRouter, status, UploadFile, Depends

from app.exceptions import CannotProcessCSV, CannotAddDataToDatabase
from app.importer.utils import convert_from_csv_to_postgres, TABLE_MODEL_MAP
from app.auth.dependencies import get_current_user

import_router = APIRouter(
    prefix="/import",
    tags=["Import csv to db"]
)


@import_router.post("/{table_name}", status_code=status.HTTP_201_CREATED, dependencies=[Depends(get_current_user)])
async def import_csv_to_db(
    tabel_name: Literal["bookings", "hotels", "rooms", "users"],
    csv_file: UploadFile
):
    content = csv.DictReader(codecs.iterdecode(csv_file.file, "utf-8"), delimiter=";")
    table = TABLE_MODEL_MAP[tabel_name]

    data = convert_from_csv_to_postgres(content)
    csv_file.file.close()
    if not data:
        raise CannotProcessCSV
    added_data = await table.add_bulk(data)
    if not added_data:
        raise CannotAddDataToDatabase
