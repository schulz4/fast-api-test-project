from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from prometheus_fastapi_instrumentator import Instrumentator
from redis import asyncio as aioredis
from sqladmin import Admin
from fastapi.middleware.cors import CORSMiddleware

from app.admin.auth import authentication_backend
from app.admin.views import BookingsAdmin, HotelsAdmin, RoomsAdmin, UsersAdmin
from app.auth.router import auth_router
from app.bookings.router import booking_router
from app.config import settings
from app.database import engine
from app.hotels.router import hotels_router
from app.images.router import images_router
from app.importer.router import import_router
from app.users.router import users_router

app = FastAPI()
admin = Admin(app, engine, authentication_backend=authentication_backend)

# CORS middleware
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://frrsdd"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "DELETE", "PATCH", "PUT"],
    allow_headers=[
        "Content-Type", "Set-Cookie", "Access-Control-Allow-Headers",
        "Access-Control-Allow-Origin", "Authorization"
    ],
    max_age=1
)

# prometheus
instrumentator = Instrumentator(
    should_group_status_codes=False,
    excluded_handlers=[".*admin.*", "/metrics"]
)
instrumentator.instrument(app).expose(app, include_in_schema=False)

# static
app.mount("/static", StaticFiles(directory="app/static"), "static")

app.include_router(users_router)
app.include_router(booking_router)
app.include_router(hotels_router)
app.include_router(images_router)
app.include_router(import_router)
app.include_router(auth_router)

admin.add_view(UsersAdmin)
admin.add_view(BookingsAdmin)
admin.add_view(RoomsAdmin)
admin.add_view(HotelsAdmin)


@app.on_event("startup")
async def startup():
    redis = aioredis.from_url(settings.redis.redis_url, encoding="utf8", decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="cache")
