from datetime import date
from typing import List, Annotated

from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm

from app.auth.auth import oauth2_scheme
from app.bookings.repository import BookingsRepo
from app.bookings.schemas import SBooking, SBookingInfo
from app.exceptions import RoomCanNotBeBooked
from app.tasks.tasks import email_confirmation
from app.auth.dependencies import get_current_user
from app.users.models import Users

booking_router = APIRouter(prefix="/booking", tags=["Booking"])


@booking_router.get("")
async def get_bookings(user: Users = Depends(get_current_user)) -> List[SBookingInfo]:
    bookings = await BookingsRepo.find_all_bookings(user.id)
    return bookings


@booking_router.post("")
async def add_booking(
        room_id: int,
        date_from: date,
        date_to: date,
        user: Users = Depends(get_current_user)
        ) -> SBooking:
    new_booking = await BookingsRepo.add(user.id, room_id, date_from, date_to)
    if not new_booking:
        raise RoomCanNotBeBooked

    email_confirmation.delay(dict(new_booking), user.email)
    return new_booking


@booking_router.delete("/{booking_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_booking(booking_id: int, user: Users = Depends(get_current_user)):
    await BookingsRepo.delete(id=booking_id, user_id=user.id)


@booking_router.post("/token")
async def test(form_data: Annotated[OAuth2PasswordRequestForm, Depends()]) -> dict:
    username = form_data.username
    return {"success": f"{username}"}


@booking_router.get("/getget")
async def getget(token: Annotated[str, Depends(oauth2_scheme)]) -> dict:

    return {"success": "success"}