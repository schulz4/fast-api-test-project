from datetime import date

from sqlalchemy import and_, func, insert, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.bookings.models import Bookings
from app.dao.base import BaseRepo
from app.database import async_session_maker
from app.hotels.models import Rooms


class BookingsRepo(BaseRepo):
    model = Bookings

    @classmethod
    async def add(cls, user_id: int, room_id: int, date_from: date, date_to: date):
        async with async_session_maker() as session:
            session: AsyncSession
            booked_rooms = (
                select(Bookings)
                .where(
                    and_(
                        Bookings.room_id == room_id,
                        and_(
                            Bookings.date_from <= date_to, Bookings.date_to >= date_from
                        ),
                    )
                )
                .cte("booked_rooms")
            )

            rooms_left = (
                select(Rooms.quantity - func.count(booked_rooms.c.room_id))
                .select_from(Rooms)
                .join(booked_rooms, booked_rooms.c.room_id == Rooms.id, isouter=True)
                .where(Rooms.id == room_id)
                .group_by(Rooms.quantity, booked_rooms.c.room_id)
            )

            get_left_rooms = await session.execute(rooms_left)

            left_rooms = get_left_rooms.scalar()

            if left_rooms > 0:
                get_room = select(Rooms).where(Rooms.id == room_id)
                result = await session.execute(get_room)
                room: Rooms = result.scalar()

                add_booking = (
                    insert(Bookings)
                    .values(
                        user_id=user_id,
                        room_id=room_id,
                        date_from=date_from,
                        date_to=date_to,
                        price=room.price,
                    )
                    .returning(
                        Bookings.id,
                        Bookings.user_id,
                        Bookings.room_id,
                        Bookings.date_from,
                        Bookings.date_to,
                        Bookings.price,
                        Bookings.total_days,
                        Bookings.total_cost,
                    )
                )

                result = await session.execute(add_booking)
                await session.commit()
                return result.mappings().first()
            else:
                return False

    @classmethod
    async def find_all_bookings(cls, user_id):
        async with async_session_maker() as session:
            session: AsyncSession

            get_bookings = (
                select(
                    Bookings.__table__.columns,
                    Rooms.image_id,
                    Rooms.name,
                    Rooms.description,
                    Rooms.services,
                )
                .join(Rooms, Rooms.id == Bookings.room_id)
                .where(Bookings.user_id == user_id)
            )

            result = await session.execute(get_bookings)

            return result.mappings().all()
