from dataclasses import dataclass
from typing import Literal

from pydantic import BaseSettings


class BaseConfig(BaseSettings):
    MODE: Literal["DEV", "TEST", "PROD"]
    LOG_LEVEL: Literal["DEBUG", "INFO", "WARN", "ERROR", "FATAL"]

    class Config:
        env_file = ".env"


class DBSetting(BaseConfig):
    DB_HOST: str
    DB_NAME: str
    DB_PORT: int
    DB_PASS: str
    DB_USER: str

    @property
    def database_url(self):
        user = f"{self.DB_USER}:{self.DB_PASS}"
        database = f"{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"
        return f"postgresql+asyncpg://{user}@{database}"


class TestDBSetting(BaseConfig):
    TEST_DB_HOST: str
    TEST_DB_NAME: str
    TEST_DB_PORT: int
    TEST_DB_PASS: str
    TEST_DB_USER: str

    @property
    def test_database_url(self):
        user = f"{self.TEST_DB_USER}:{self.TEST_DB_PASS}"
        database = f"{self.TEST_DB_HOST}:{self.TEST_DB_PORT}/{self.TEST_DB_NAME}"
        return f"postgresql+asyncpg://{user}@{database}"


class AuthSetting(BaseConfig):
    ALGORITHM: str
    KEY: str


class RedisSetting(BaseConfig):
    REDIS_HOST: str
    REDIS_PORT: int

    @property
    def redis_url(self):
        return f"redis://{self.REDIS_HOST}:{self.REDIS_PORT}"


class SMTPSetting(BaseConfig):
    SMTP_PASS: str
    SMTP_HOST: str
    SMTP_PORT: int
    SMTP_USER: str


@dataclass
class Settings:
    base: BaseConfig
    db_setting: DBSetting
    test_db_setting: TestDBSetting
    auth: AuthSetting
    redis: RedisSetting
    smtp: SMTPSetting


settings = Settings(
    base=BaseConfig(),
    db_setting=DBSetting(),
    test_db_setting=TestDBSetting(),
    auth=AuthSetting(),
    redis=RedisSetting(),
    smtp=SMTPSetting(),
)
