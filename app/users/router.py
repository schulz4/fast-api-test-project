from fastapi import APIRouter, Depends, Response, status

# from app.auth.misc import authenticate_user, create_access_token, get_password_hash
from app.users.repository import UsersRepo
from app.auth.dependencies import get_current_user
from app.users.models import Users
from app.users.schemas import SUser, SUserAuth

users_router = APIRouter(prefix="/users", tags=["Users"])


# @users_router.post("/register", status_code=status.HTTP_201_CREATED)
# async def register_user(user_data: SUserAuth):
#     existing_user = await UsersRepo.find_one_or_none(email=user_data.email)
#     if existing_user:
#         raise UserAlreadyExistsException
#
#     hashed_password = get_password_hash(user_data.password)
#     await UsersRepo.add(
#         email=user_data.email,
#         hashed_password=hashed_password
#     )
#
#
# @users_router.post("/login")
# async def login_user(response: Response, user_data: SUserAuth):
#     user: Users = await authenticate_user(user_data.email, user_data.password)
#     if not user:
#         raise IncorrectEmailOrPasswordException
#
#     data = {
#         "sub": str(user.id)
#     }
#     access_token = create_access_token(data, expire_min=3600)
#     response.set_cookie("booking_access_token", access_token, httponly=True)
#     return {"login": "success"}
#
#
# @users_router.post("/logout")
# async def logout_user(response: Response) -> dict:
#     response.delete_cookie("booking_access_token")
#     return {"response": "user logout"}


@users_router.get("/me")
async def get_me(current_user: Users = Depends(get_current_user)) -> SUser:
    return current_user
