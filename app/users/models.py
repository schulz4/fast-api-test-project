import uuid

from sqlalchemy import Boolean, Column, Integer, String, UUID
from sqlalchemy.orm import relationship

from app.database import Base


class Users(Base):
    __tablename__ = "users"

    id = Column(UUID, primary_key=True, default=uuid.uuid4)
    email = Column(String)
    hashed_password = Column(String)
    is_admin = Column(Boolean, default=False)

    bookings = relationship("Bookings", back_populates="user")

    def __str__(self):
        return f"Пользователь: {self.email}"
