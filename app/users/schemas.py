from uuid import UUID

from pydantic import BaseModel, EmailStr


class SUserAuth(BaseModel):
    email: EmailStr
    password: str


class SUser(BaseModel):
    id: UUID
    email: EmailStr
    hashed_password: str

    class Config:
        orm_mode = True
