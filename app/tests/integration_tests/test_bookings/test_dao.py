from datetime import datetime

from app.bookings.repository import BookingsDAO


async def test_add_and_get_bookings():
    new_booking = await BookingsDAO.add(
        user_id=2,
        room_id=2,
        date_from=datetime.strptime("2023-05-01", "%Y-%m-%d"),
        date_to=datetime.strptime("2023-05-15", "%Y-%m-%d")
    )

    assert new_booking.user_id == 2
    assert new_booking.room_id == 2

    new_booking = await BookingsDAO.find_one_or_none(id=new_booking.id)

    assert new_booking is not None

    await BookingsDAO.delete(id=new_booking.id)

    booking = await BookingsDAO.find_one_or_none(id=new_booking.id)

    assert booking is None
