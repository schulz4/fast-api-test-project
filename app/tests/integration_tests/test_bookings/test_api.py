import pytest
from httpx import AsyncClient

except_booking = [
    (10, "2023-05-01", "2023-05-15", 3, 200),
    (10, "2023-05-01", "2023-05-15", 4, 200),
    (10, "2023-05-01", "2023-05-15", 5, 200),
]


@pytest.mark.parametrize("room_id,date_from,date_to,booked_rooms,status", [
    *except_booking,
    (10, "2023-05-01", "2023-05-15", 5, 409),
    (10, "2023-05-01", "2023-05-15", 5, 409)
])
async def test_add_and_get_bookings(room_id, date_from, date_to, booked_rooms, status, authorized_client: AsyncClient):
    response = await authorized_client.post("/booking", params={
        "room_id": room_id,
        "date_from": date_from,
        "date_to": date_to,
    })

    assert response.status_code == status

    response = await authorized_client.get("/booking")

    assert len(response.json()) == booked_rooms


async def test_get_and_delete_bookings(authorized_client: AsyncClient):
    response = await authorized_client.get("/booking")

    if response.status_code == 200:
        for booking in response.json():
            await authorized_client.delete(f"/booking/{booking['id']}")

    response = await authorized_client.get("/booking")

    assert len(response.json()) == 0
