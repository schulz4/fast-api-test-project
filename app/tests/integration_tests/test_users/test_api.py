import pytest
from httpx import AsyncClient


@pytest.mark.parametrize("email,password,status", [
    ("testing@testing.com", "testing", 200),
    ("testing2@testing.com", "testing2", 200),
    ("testing@testing.com", "testing", 409),
    ("qwerty", "testing", 422)
])
async def test_register_user(email, password, status, async_client: AsyncClient):
    response = await async_client.post("/auth/register", json={
        "email": email,
        "password": password
    })

    assert response.status_code == status


@pytest.mark.parametrize("email,password,status", [
    ("test@test.com", "test", 200),
    ("artem@example.com", "artem", 200),
    ("notexist@example.com", "artem", 401)
])
async def test_login_user(email, password, status, async_client: AsyncClient):
    response = await async_client.post("/auth/login", json={
        "email": email,
        "password": password
    })

    assert response.status_code == status


async def test_user_me(authorized_client):
    response = await authorized_client.get("users/me")
    print(response.json())
    print(authorized_client.headers.get("authorization"))
