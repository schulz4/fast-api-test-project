import pytest
from httpx import AsyncClient


@pytest.mark.parametrize("location,date_from,date_to,status,hotels_total", [
    ("Алтай", "2023-05-15", "2023-05-01", 409, 0),
    ("Алтай", "2023-05-15", "2023-08-01", 409, 0),
    ("Алтай", "2023-05-15", "2023-05-22", 200, 3),
])
async def test_get_hotels_by_location(location, date_from, date_to, status, hotels_total, async_client: AsyncClient):
    response = await async_client.get(f"/hotels/{location}", params={
        "date_from": date_from,
        "date_to": date_to
    })

    assert response.status_code == status
    if response.status_code == 200:
        assert len(response.json()) == hotels_total
