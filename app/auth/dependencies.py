from typing import Annotated

from fastapi import Depends

from app.auth.auth import oauth2_scheme
from app.auth.misc import check_jwt
from app.auth.exceptions import NotUserID
from app.users.repository import UsersRepo


async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):

    user_id = check_jwt(token)

    user = await UsersRepo.find_one_or_none(id=user_id)
    if not user:
        raise NotUserID

    return user
