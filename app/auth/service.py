from datetime import datetime, timedelta

from jose import jwt
from pydantic import EmailStr
from sqladmin.exceptions import SQLAdminException

from app.auth.exceptions import TokenNotAddedException, UserHasNotSession, InvalidRefreshToken
from app.auth.misc import verify_password
from app.auth.repository import RefreshSessionsRepo
from app.auth.schemas import TokenInfo
from app.config import settings
from app.users.models import Users
from app.users.repository import UsersRepo


class AuthorizationService:
    def __init__(self):
        self.sessions_repo = RefreshSessionsRepo()
        self.users_repo = UsersRepo()

    @staticmethod
    def create_token(data: dict, exp_min: int) -> TokenInfo:
        to_encode = data.copy()
        expire = datetime.utcnow() + timedelta(minutes=exp_min)
        to_encode.update(exp=expire)
        encode_jwt = jwt.encode(
            to_encode, settings.auth.KEY, settings.auth.ALGORITHM
        )
        return TokenInfo(token=encode_jwt, expires_in=expire)

    async def authenticate_user(self, email: EmailStr, password: str):
        user: Users = await self.users_repo.find_one_or_none(email=email)
        if not user or not verify_password(password, user.hashed_password):
            return None
        return user

    async def create_refresh_session(self, **values):
        try:
            await self.sessions_repo.add(**values)
        except SQLAdminException:
            raise TokenNotAddedException

    async def check_refresh_token(self, token: str, ip: str, user_id: str) -> bool:
        session = await self.sessions_repo.find_one_or_none(refresh_token=token, user_id=user_id)
        if not session:
            raise UserHasNotSession

        await self.sessions_repo.delete(id=session.id)

        if session.ip != ip:
            raise InvalidRefreshToken

        return True

    @staticmethod
    def rrr():
        return f"hi"

