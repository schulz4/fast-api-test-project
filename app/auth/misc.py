from datetime import datetime, timedelta
from uuid import UUID

from jose import jwt, ExpiredSignatureError, JWTError
from passlib.context import CryptContext
from pydantic import EmailStr

from app.auth.exceptions import TokenNotCorrectException, TokenExpiredException
from app.config import settings
from app.users.repository import UsersRepo
from app.users.models import Users

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def check_jwt(token: str) -> str:
    try:
        payload = jwt.decode(token, settings.auth.KEY, settings.auth.ALGORITHM)
        user_id = payload.get("sub")
        if not user_id:
            raise TokenNotCorrectException

        return user_id

    except (ExpiredSignatureError, JWTError) as e:
        if isinstance(e, ExpiredSignatureError):
            raise TokenExpiredException
        else:
            raise TokenNotCorrectException
