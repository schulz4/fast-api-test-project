from typing import Annotated

from fastapi import APIRouter, Depends, Request, Response
from fastapi.security import OAuth2PasswordRequestForm

from app.auth.misc import check_jwt
from app.auth.service import AuthorizationService
from app.auth.exceptions import IncorrectEmailOrPasswordException, TokenAbsentException

auth_router = APIRouter(
    prefix="/auth",
    tags=["Auth"]
)


@auth_router.post("/login")
async def get_access_token(
        form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
        response: Response,
        request: Request
) -> dict:
    user = await AuthorizationService().authenticate_user(form_data.username, form_data.password)

    if not user:
        raise IncorrectEmailOrPasswordException

    data = {
        "sub": str(user.id)
    }

    access_token = AuthorizationService().create_token(data=data, exp_min=1)
    refresh_token = AuthorizationService().create_token(data=data, exp_min=3600)

    response.set_cookie(
        key="refresh",
        value=refresh_token.token,
        max_age=3600,
        path="/auth",
        httponly=True
    )

    await AuthorizationService().create_refresh_session(
        user_id=user.id,
        refresh_token=refresh_token.token,
        ip=request.client.host,
        expires_in=refresh_token.expires_in
    )

    return {
        "access_token": access_token.token,
        "refresh_token": refresh_token.token,
        "token_type": "Bearer"
    }


@auth_router.post("/refreshToken")
async def get_refresh_token(request: Request, response: Response) -> dict:
    token = request.cookies.get("refresh")
    ip = request.client.host
    user_id = check_jwt(token)

    if not token:
        raise TokenAbsentException

    await AuthorizationService().check_refresh_token(token, ip, user_id)

    data = {
        "sub": user_id
    }

    access_token = AuthorizationService().create_token(data=data, exp_min=1)
    refresh_token = AuthorizationService().create_token(data=data, exp_min=3600)

    response.set_cookie(
        key="refresh",
        value=refresh_token.token,
        max_age=3600,
        path="/auth",
        httponly=True
    )

    await AuthorizationService().create_refresh_session(
        user_id=user_id,
        refresh_token=refresh_token.token,
        ip=request.client.host,
        expires_in=refresh_token.expires_in
    )

    return {
        "access_token": access_token.token,
        "refresh_token": refresh_token.token,
        "token_type": "Bearer"
    }


@auth_router.post("/logout")
async def tt(response: Response) -> dict:
    response.delete_cookie("refresh", path="auth/", httponly=True)
    return {
        "logout": "success"
    }
