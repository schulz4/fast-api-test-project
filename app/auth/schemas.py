from datetime import date

from pydantic import BaseModel


class SToken(BaseModel):
    access_token: str
    token_type: str


class TokenInfo(BaseModel):
    token: str
    expires_in: date
