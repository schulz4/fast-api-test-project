from datetime import datetime

from sqlalchemy import Column, Integer, ForeignKey, UUID, String, BIGINT, DateTime

from app.database import Base


class RefreshSessions(Base):
    __tablename__ = "refresh_sessions"

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey("users.id", ondelete="CASCADE"))
    refresh_token = Column(String, nullable=False)
    ip = Column(String(length=15))
    expires_in = Column(DateTime, nullable=False)
    created_at = Column(DateTime(timezone=True), default=datetime.utcnow)
