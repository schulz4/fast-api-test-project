from app.auth.models import RefreshSessions
from app.dao.base import BaseRepo


class RefreshSessionsRepo(BaseRepo):
    model = RefreshSessions
