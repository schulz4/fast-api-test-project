import smtplib
from pathlib import Path

from PIL import Image
from pydantic import EmailStr

from app.bookings.schemas import SBooking
from app.config import settings
from app.tasks.celery_app import celery_app
from app.tasks.email_templates import create_booking_confirmation_template


@celery_app.task
def process_pic(path: str):
    img_path = Path(path)
    img = Image.open(img_path)
    img_mid = img.resize((1000, 500))
    img_small = img.resize((200, 100))

    img_mid.save(f"app/static/images/midsize_{img_path.name}")
    img_small.save(f"app/static/images/smallsize_{img_path.name}")


@celery_app.task
def email_confirmation(booking: SBooking, email_to: EmailStr):
    email_to_mock = settings.smtp.SMTP_USER
    msg_content = create_booking_confirmation_template(booking, email_to_mock)

    with smtplib.SMTP_SSL(settings.smtp.SMTP_HOST, settings.smtp.SMTP_PORT) as server:
        server.login(settings.smtp.SMTP_USER, settings.smtp.SMTP_PASS)

        server.send_message(msg_content)
