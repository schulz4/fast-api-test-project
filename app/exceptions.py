from fastapi import HTTPException, status


class BookingException(HTTPException):
    status_code = 500
    detail = ""
    headers = None

    def __init__(self):
        super().__init__(status_code=self.status_code, detail=self.detail, headers=self.headers)


class RoomCanNotBeBooked(BookingException):
    status_code = status.HTTP_409_CONFLICT
    detail = "There are no empty rooms"


class DateFromCanNotBeAfterDateTo(BookingException):
    status_code = status.HTTP_409_CONFLICT
    detail = "Date from can't be after date to"


class TooLongPeriodException(BookingException):
    status_code = status.HTTP_409_CONFLICT
    detail = "Too log period for booking"


class CannotProcessCSV(BookingException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    detail = "Не удалось обработать CSV файл"


class CannotAddDataToDatabase(BookingException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    detail = "Не удалось добавить запись"
