from sqladmin import ModelView

from app.bookings.models import Bookings
from app.hotels.models import Hotels, Rooms
from app.users.models import Users


class UsersAdmin(ModelView, model=Users):
    column_list = [Users.id, Users.email]
    column_details_list = [Users.id, Users.email, Users.bookings]
    can_delete = False
    name = "Пользователь"
    name_plural = "Пользователи"
    icon = "fa-solid fa-user"


class BookingsAdmin(ModelView, model=Bookings):
    column_list = [
        Bookings.id, Bookings.user,
        Bookings.date_from, Bookings.date_to,
        Bookings.price, Bookings.total_days,
        Bookings.total_cost
    ]
    name = "Букинг"
    name_plural = "Букинги"
    icon = "fa-solid fa-list"


class RoomsAdmin(ModelView, model=Rooms):
    column_list = [
        Rooms.id, Rooms.name,
        Rooms.price, Rooms.quantity,
        Rooms.hotel
    ]

    name = "Комната"
    name_plural = "Комнаты"
    icon = "fa-solid fa-bed"


class HotelsAdmin(ModelView, model=Hotels):
    column_list = [
        Hotels.id, Hotels.name,
        Hotels.location, Hotels.rooms_quantity
    ]

    name = "Оетель"
    name_plural = "Отели"
    icon = "fa-solid fa-hotel"
