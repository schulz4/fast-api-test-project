from datetime import date

from sqlalchemy import and_, func, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.bookings.models import Bookings
from app.dao.base import BaseRepo
from app.database import async_session_maker
from app.hotels.models import Hotels, Rooms


class HotelsRepo(BaseRepo):
    model = Hotels

    @classmethod
    async def find_by_location(
            cls,
            location: str,
            date_from: date,
            date_to: date
    ):
        async with async_session_maker() as session:
            session: AsyncSession

            booked_rooms = select(Bookings.room_id, func.count(Bookings.room_id).label("rooms_booked")) \
                .select_from(Bookings) \
                .where(and_(Bookings.date_from <= date_to, Bookings.date_to >= date_from)) \
                .group_by(Bookings.room_id) \
                .cte("booked_rooms")

            booked_hotels = select(
                Rooms.hotel_id,
                func.sum(Rooms.quantity - func.coalesce(booked_rooms.c.rooms_booked, 0)).label("rooms_left")) \
                .select_from(Rooms) \
                .join(booked_rooms, booked_rooms.c.room_id == Rooms.id, isouter=True) \
                .group_by(Rooms.hotel_id) \
                .cte("booked_hotels")

            get_hotels_with_rooms = select(
                Hotels.__table__.columns,
                booked_hotels.c.rooms_left) \
                .join(booked_hotels, booked_hotels.c.hotel_id == Hotels.id, isouter=True) \
                .where(
                    and_(
                        booked_hotels.c.rooms_left > 0,
                        Hotels.location.ilike(f"%{location}%")
                    )
                )

            result = await session.execute(get_hotels_with_rooms)
            hotels = result.mappings().all()
            return hotels


class RoomsRepo(BaseRepo):
    model = Rooms

    @classmethod
    async def find_all(
            cls,
            hotel_id,
            date_from,
            date_to
    ):
        async with async_session_maker() as session:
            session: AsyncSession
            booked_rooms = select(Bookings.room_id, func.count(Bookings.room_id).label("booked_rooms")) \
                .select_from(Bookings) \
                .where(and_(Bookings.date_from <= date_to, Bookings.date_to >= date_from)) \
                .group_by(Bookings.room_id) \
                .cte("booked_rooms")

            get_rooms = select(
                Rooms.__table__.columns,
                ((date_to - date_from).days * Rooms.price).label("total_cost"),
                (Rooms.quantity - func.coalesce(booked_rooms.c.booked_rooms, 0)).label("rooms_left")) \
                .join(booked_rooms, booked_rooms.c.room_id == Rooms.id, isouter=True) \
                .where(Rooms.hotel_id == hotel_id)

            result = await session.execute(get_rooms)
            rooms = result.mappings().all()

            return rooms
