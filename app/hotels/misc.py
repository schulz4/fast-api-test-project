from app.exceptions import DateFromCanNotBeAfterDateTo, TooLongPeriodException


def check_dates(date_from, date_to):
    if date_from > date_to:
        raise DateFromCanNotBeAfterDateTo
    if (date_to - date_from).days > 31:
        raise TooLongPeriodException

    return
