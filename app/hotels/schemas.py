from typing import List

from pydantic import BaseModel


class SHotels(BaseModel):
    id: int
    name: str
    location: str
    services: List[str]
    rooms_quantity: int
    image_id: int

    class Config:
        orm_mode = True


class SHotelsInfo(SHotels):
    rooms_left: int


class SRooms(BaseModel):
    id: int
    hotel_id: int
    name: str
    description: str
    price: int
    services: List[str]
    quantity: int
    image_id: int

    class Config:
        orm_mode = True


class SRoomsInfo(SRooms):
    total_cost: int
    rooms_left: int
