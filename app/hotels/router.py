from datetime import date, datetime, timedelta
from typing import List

from fastapi import APIRouter, Query
from fastapi_cache.decorator import cache

from app.hotels.repository import HotelsRepo, RoomsRepo
from app.hotels.schemas import SHotels, SHotelsInfo, SRoomsInfo
from app.hotels.misc import check_dates

hotels_router = APIRouter(
    prefix="/hotels",
    tags=["Hotels & Rooms"]
)


@hotels_router.get("/id/{hotel_id}")
async def get_hotel_by_id(hotel_id: int) -> SHotels:
    hotel = await HotelsRepo.find_one_or_none(id=hotel_id)
    return hotel


@hotels_router.get("/{location}")
@cache(expire=180)
async def get_hotels_by_location(
        location: str,
        date_from: date = Query(description=f"Например, {datetime.now().date()}"),
        date_to: date = Query(description=f"Например, {(datetime.now() + timedelta(days=14)).date()}")
) -> List[SHotelsInfo]:
    check_dates(date_from, date_to)

    hotels = await HotelsRepo.find_by_location(location, date_from, date_to)
    return hotels


@hotels_router.get("/{hotel_id}/rooms")
async def get_rooms_by_hotel(
        hotel_id: int,
        date_from: date = Query(description=f"Например, {datetime.now().date()}"),
        date_to: date = Query(description=f"Например, {(datetime.now() + timedelta(days=14)).date()}")
) -> List[SRoomsInfo]:
    check_dates(date_from, date_to)

    rooms = await RoomsRepo.find_all(hotel_id=hotel_id, date_from=date_from, date_to=date_to)

    return rooms
