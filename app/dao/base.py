from sqlalchemy import delete, insert, select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from app.database import async_session_maker
from app.logger import logger


class BaseRepo:
    model = None

    @classmethod
    async def get_all(cls, *clauses):
        async with async_session_maker() as session:
            session: AsyncSession
            query = select(cls.model).where(*clauses)
            result = await session.execute(query)
            return result.scalars().all()

    @classmethod
    async def find_one_or_none(cls, **filter_by):
        async with async_session_maker() as session:
            session: AsyncSession
            query = select(cls.model).filter_by(**filter_by)
            result = await session.execute(query)
            return result.scalar_one_or_none()

    @classmethod
    async def add(cls, **values):
        async with async_session_maker() as session:
            session: AsyncSession
            query = insert(cls.model).values(**values)
            await session.execute(query)
            await session.commit()

    @classmethod
    async def delete(cls, **clauses):
        async with async_session_maker() as session:
            session: AsyncSession
            query = delete(cls.model).filter_by(**clauses)
            await session.execute(query)
            await session.commit()

    @classmethod
    async def add_bulk(cls, *data):
        try:
            async with async_session_maker() as session:
                session: AsyncSession
                query = insert(cls.model).values(*data).returning(cls.model.id)
                result = await session.execute(query)
                await session.commit()
                return result.mappings().first()
        except (SQLAlchemyError, Exception) as e:
            if isinstance(e, SQLAlchemyError):
                msg = "Database Exc"
            elif isinstance(e, Exception):
                msg = "Unknown Exc"

            msg += ": Cannot bulk insert data into table"

            logger.error(msg, extra={"table": cls.model.__tablename__}, exc_info=True)
            return None
