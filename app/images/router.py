import shutil

from fastapi import APIRouter, UploadFile

from app.tasks.tasks import process_pic

images_router = APIRouter(
    prefix="/image",
    tags=["Images"]
)


@images_router.post("/hotels")
async def upload_image(name: int, file: UploadFile):
    path = f"app/static/images/{name}.webp"
    with open(path, "wb+") as file_object:
        shutil.copyfileobj(file.file, file_object)

    process_pic.delay(path)
